import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Random;

public class AccesoDeDatosTest {

    AccesoDeDatos accesoDeDatos;

    @Test
    public void leeJornada(){
        accesoDeDatos = AccesoDeDatos.getInstance();
        Jornada jornada = accesoDeDatos.leeJornada(1);
        System.out.println(jornada.numeroDeJornada);
        for (Partido partido : jornada.partidosJugados) {
            System.out.println(partido);
        }
        Assert.assertNotNull(jornada);
    }

    @Test
    public void guardaJornada() {
        accesoDeDatos = AccesoDeDatos.getInstance();
        Random r = new Random();
        Jornada jornada = new Jornada();
        jornada.numeroDeJornada = r.nextInt();
        jornada.partidosJugados = new ArrayList<>();
        Partido partido = new Partido();
        partido.setEquipoLocal("Mi equipo");
        partido.setGolesLocal(2);
        partido.setEquipoVisitante("El visitante");
        partido.setGolesVisitante(3);
        Partido partido2 = new Partido();
        partido2.setEquipoLocal("Equipo");
        partido2.setGolesLocal(1);
        partido2.setEquipoVisitante("Visitantes");
        partido2.setGolesVisitante(0);
        jornada.partidosJugados.add(partido);
        jornada.partidosJugados.add(partido2);
        accesoDeDatos.guardaJornada(jornada);
    }

}
