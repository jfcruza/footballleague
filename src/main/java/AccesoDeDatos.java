import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class AccesoDeDatos {

    private static final AccesoDeDatos instance = new AccesoDeDatos();
    private AccesoDeDatos() {}
    public static AccesoDeDatos getInstance() { return instance; }

    private FileWriter fw;
    private FileReader fr;
    private File folder = new File("src/main/resources/txtJornada");

    void guardaJornada(Jornada jornada) {
        String archivoJornada = "src/main/resources/txtJornada/" + jornada.numeroDeJornada + ".txt";
        try {
            fw = new FileWriter(archivoJornada);
            BufferedWriter bw = new BufferedWriter(fw);
            for (Partido partido : jornada.partidosJugados) {
                bw.write(partido.toString());
                bw.newLine();
            }
            bw.flush();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    Jornada leeJornada(int numeroDeJornada) {
        String archivoJornada = "src/main/resources/txtJornada/" + numeroDeJornada + ".txt";
        Jornada jornada = leeArchivoDeJornada(archivoJornada);
        if (jornada != null)
            jornada.numeroDeJornada = numeroDeJornada;
        return jornada;
    }

    Jornada leeArchivoDeJornada(String archivo) {
        Jornada jornada = new Jornada();
        jornada.partidosJugados = new ArrayList<>();
        String linea;
        try {
            fr = new FileReader(archivo);
            BufferedReader br = new BufferedReader(fr);
            while((linea = br.readLine()) != null) {
                String[] datos = linea.split(":");
                Partido partido = new Partido();
                partido.setEquipoLocal(datos[0]);
                partido.setGolesLocal(Integer.parseInt(datos[1]));
                partido.setEquipoVisitante(datos[2]);
                partido.setGolesVisitante(Integer.parseInt(datos[3]));
                jornada.partidosJugados.add(partido);
            }
            return jornada;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("La jornada no existe aún");
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error al leer el archivo");
            return null;
        }
    }

    List<Jornada> consultaTodasLasJornadas() {
        File[] files = folder.listFiles();
        List<Jornada> jornadas = new ArrayList<Jornada>();
        for (File file : files) {
            if (file.isFile()) {
                Jornada jornada = leeArchivoDeJornada(file.getAbsolutePath());
                jornada.numeroDeJornada = Integer.parseInt(file.getName().split("\\.")[0]);
                jornadas.add(jornada);
            }
        }
        return jornadas;
    }

}
