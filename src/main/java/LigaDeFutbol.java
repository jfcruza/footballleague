import java.util.Scanner;

public class LigaDeFutbol {

  private static ServicioDeJornadas servicioDeJornadas;

  public static void main(String[] args) {

    int option;
    Scanner scanner = new Scanner(System.in);
    servicioDeJornadas = new ServicioDeJornadas();

    do {
      System.out.println();
      System.out.println("Elige una opción:");
      System.out.println("1.- Ingresar Jornada nueva");
      System.out.println("2.- Revisar jornada");
      System.out.println("3.- Ganadores locales");
      System.out.println("4.- Goles por jornada");
      System.out.println("5.- Ganadores visitantes");
      System.out.println("6.- Consultar todas las jornadas");
      System.out.println("0.- Salir");
      option = scanner.nextInt();
      doOption(option);
    } while (option != 0);

  }

  public static void doOption(int option) {
    switch (option) {
      case 1:
        servicioDeJornadas.nuevaJornada();
        break;
      case 2:
        servicioDeJornadas.consultaJornada();
        break;
      case 3:
        servicioDeJornadas.imprimeGanadoresLocalesTotales();
        break;
      case 6:
        servicioDeJornadas.consultaTodasLasJornadas();
        break;
    }
  }

}
