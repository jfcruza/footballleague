import java.util.*;

public class ServicioDeJornadas {

  private Scanner scanner = new Scanner(System.in);
  private AccesoDeDatos accesoDeDatos = AccesoDeDatos.getInstance();

  public Jornada nuevaJornada() {
    int option;
    Jornada jornada = new Jornada();
    jornada.partidosJugados = new ArrayList<Partido>();
    System.out.println("Ingresa el número de jornada");
    jornada.numeroDeJornada = scanner.nextInt();
    do {
      System.out.println("Ingresar nuevo partido: ");
      System.out.println("1 - Si");
      System.out.println("0 - No");
      option = scanner.nextInt();
      if (option == 1) {
        Partido partido = new Partido();
        System.out.println("Ingresa equipo local");
        partido.setEquipoLocal(scanner.next("[a-zA-Z0-9 ]+"));
        System.out.println("Ingresa goles locales");
        partido.setGolesLocal(scanner.nextInt());
        System.out.println("Ingresa equipo visitante");
        partido.setEquipoVisitante(scanner.next());
        System.out.println("Ingresa goles visitante");
        partido.setGolesVisitante(scanner.nextInt());
        jornada.partidosJugados.add(partido);
      }
    } while (option != 0);
    accesoDeDatos.guardaJornada(jornada);
    return jornada;
  }

  public void consultaJornada() {
    int numeroJornada;
    System.out.println("Ingresa el numero de jornada");
    numeroJornada = scanner.nextInt();
    Jornada jornada = accesoDeDatos.leeJornada(numeroJornada);
    if (jornada != null)
      System.out.print(jornada);
  }

  public void consultaTodasLasJornadas() {
    List<Jornada> jornadas = accesoDeDatos.consultaTodasLasJornadas();
    for (Jornada jornada : jornadas) {
      System.out.println();
      System.out.print(jornada);
    }
  }

  public List<String> ganadoresLocalesDeJornada(Jornada jornada) {
    List<String> ganadoresLocales = new ArrayList<>();
    for (Partido partido : jornada.partidosJugados) {
      if (partido.getGolesLocal() > partido.getGolesVisitante())
        ganadoresLocales.add(partido.getEquipoLocal());
    }
    return ganadoresLocales;
  }

  public String[] ganadoresLocalesTotales() {
    List<Jornada> jornadas = accesoDeDatos.consultaTodasLasJornadas();
    Set<String> ganadoresLocales = new HashSet<String>();
    for (Jornada jornada : jornadas) {
      ganadoresLocales.addAll(ganadoresLocalesDeJornada(jornada));
    }
    return ganadoresLocales.toArray(new String[0]);
  }

  public void imprimeGanadoresLocalesTotales() {
    for (String ganadorLocal : ganadoresLocalesTotales()) {
      System.out.println(ganadorLocal);
    }
  }
}
