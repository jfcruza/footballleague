import java.util.List;

public class Jornada {

    int numeroDeJornada;
    List<Partido> partidosJugados;

    @Override
    public String toString() {
        String toString = "Jornada: " + numeroDeJornada + "\n";
        for (Partido partido : partidosJugados) {
            toString += partido.toString() + "\n";
        }
        return toString;
    }
}
